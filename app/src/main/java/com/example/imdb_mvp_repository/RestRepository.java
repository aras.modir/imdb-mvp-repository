package com.example.imdb_mvp_repository;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestRepository implements ModelRepository.Rest {

    private Model model;

    @Override
    public void attatchModel(Model model) {

        this.model = model;
    }

    @Override
    public void getData(final String word) {
        API.getMovie().create(IMDBWebInterface.class).searchInIMDB(word, "70ad462a")
                .enqueue(new Callback<IMDBPojo>() {
                    @Override
                    public void onResponse(Call<IMDBPojo> call, Response<IMDBPojo> response) {
                        model.onRecievedData(response.body(), RepoType.Rest);
                    }

                    @Override
                    public void onFailure(Call<IMDBPojo> call, Throwable t) {
                        model.onFailed(word, RepoType.Rest);
                    }
                });
    }
}
