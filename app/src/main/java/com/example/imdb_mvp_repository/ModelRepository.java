package com.example.imdb_mvp_repository;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

public interface ModelRepository {

    interface Rest {
        void attatchModel(Model model);
        void getData(String word);
    }

    interface Database {
        void attatchModel(Model model);
        void getData(String word);
        void saveDataToDB(IMDBPojo imdbPojo);

    }


}
