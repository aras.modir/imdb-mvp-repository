package com.example.imdb_mvp_repository;

import android.nfc.Tag;
import android.util.Log;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

import java.util.List;

public class DatabaseRepository implements ModelRepository.Database {


    private Model model;
    IMDBPojo imdbPojo;

    @Override
    public void attatchModel(Model model) {
        this.model = model;
    }

    @Override
    public void getData(String word) {

        List<IMDBPojo> movies = IMDBPojo.find(IMDBPojo.class, "Title like ? ", word);

        if (movies.size() == 0) {
            model.onFailed(word, RepoType.Database);
        } else {
            IMDBPojo pojo = new IMDBPojo();
            pojo.setActors(movies.get(0).getActors());
            pojo.setPoster(movies.get(0).getPoster());
            model.onRecievedData(pojo, RepoType.Database);
        }
    }

    @Override
    public void saveDataToDB(IMDBPojo imdbPojo) {
        IMDBPojo db = new IMDBPojo();
        db.setTitle(imdbPojo.getTitle());
        db.setActors(imdbPojo.getActors());
        db.setPoster(imdbPojo.getPoster());
        db.save();
    }
}
