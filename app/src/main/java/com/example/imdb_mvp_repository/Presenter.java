package com.example.imdb_mvp_repository;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

public class Presenter implements IMDBMVPContract.Presenter {
    private IMDBMVPContract.View view;
    IMDBMVPContract.Model model = new Model();

    @Override
    public void attatchView(IMDBMVPContract.View view) {
        this.view = view;
        model.AttatchPresenter(this);

    }

    @Override
    public void search(String word) {
        view.showloading(true);
        model.search(word);

    }

    @Override
    public void onSuccessSearch(IMDBPojo imdbPojo) {
        view.showloading(false);
        view.onSuccessSearch(imdbPojo);
    }

    @Override
    public void onFail(String msg) {
        view.showloading(false);
        view.onFail(msg);
    }

    @Override
    public void validateWord(String word) {
        if (word != null) {
            search(word);
        } else {
            view.onWordNull();
        }
    }
}
