package com.example.imdb_mvp_repository;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IMDBWebInterface {
    @GET("/")
    Call<IMDBPojo>
    searchInIMDB(@Query("t") String t, @Query("apikey") String apikey);

}
