package com.example.imdb_mvp_repository;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

public class Model implements IMDBMVPContract.Model {
    private IMDBMVPContract.Presenter presenter;
    private DatabaseRepository databaseRepository = new DatabaseRepository();
    private RestRepository restRepository = new RestRepository();


    @Override
    public void AttatchPresenter(IMDBMVPContract.Presenter presenter) {
        this.presenter = presenter;
        restRepository.attatchModel(this);
        databaseRepository.attatchModel(this);
    }

    @Override
    public void search(String word) {
        databaseRepository.getData(word);
    }

    @Override
    public void onRecievedData(IMDBPojo imdbPojo, RepoType type) {
        if (type == RepoType.Rest){
            databaseRepository.saveDataToDB(imdbPojo);
            presenter.onSuccessSearch(imdbPojo);
        } else if (type == RepoType.Database) {
            presenter.onSuccessSearch(imdbPojo);
        }
    }

    @Override
    public void onFailed(String word, RepoType type) {
        if (type == RepoType.Rest){
            presenter.onFail("error in webservice call");
        } else if (type == RepoType.Database) {
            restRepository.getData(word);
        }
    }


}
