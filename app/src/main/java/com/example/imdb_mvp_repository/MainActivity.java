package com.example.imdb_mvp_repository;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements IMDBMVPContract.View {
    Presenter presenter = new Presenter();
    ProgressDialog progressDialog;
    EditText word;
    Button search;
    ImageView cover;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        word = findViewById(R.id.word);
        search = findViewById(R.id.search);
        cover = findViewById(R.id.cover);
        result = findViewById(R.id.result);

        presenter.attatchView(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait");

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateWord(word.getText().toString());
            }
        });
    }

    @Override
    public void onWordNull() {
        Toast.makeText(this, "Please fill the form", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessSearch(IMDBPojo imdbPojo) {
        showloading(false);
        result.setText(imdbPojo.getActors());
        Picasso.get().load(imdbPojo.getPoster()).into(cover);
    }

    @Override
    public void onFail(String msg) {
        showloading(false);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showloading(Boolean show) {
        if (show) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
