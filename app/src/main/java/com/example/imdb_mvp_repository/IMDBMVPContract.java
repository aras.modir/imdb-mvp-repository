package com.example.imdb_mvp_repository;

import com.example.imdb_mvp_repository.PojoModel.IMDBPojo;

public interface IMDBMVPContract {

    interface View {
        void onWordNull();
        void onSuccessSearch(IMDBPojo imdbPojo);
        void onFail(String msg);
        void showloading(Boolean show);

    }

    interface Presenter {
        void attatchView(View view);
        void search(String word);
        void onSuccessSearch(IMDBPojo imdbPojo);
        void onFail(String msg);
        void validateWord(String word);
    }

    interface Model {
        void AttatchPresenter(Presenter presenter);
        void search(String word);
        void onRecievedData(IMDBPojo imdbPojo, RepoType type);
        void onFailed(String word, RepoType type);

    }
}
